package demo.pos.persistence;

/**
 * @author Jan de Rijke.
 */
public enum PersistenceType {
	DB,
	MEMORY
}
