This is a version of the POS case (larman) with
- a Discount strategy in demo.pos.domain.sale.discount.Discount 
- JUnit 5
- A stateless controller
- a repository factory 
  - a memory repository
  - a db repository (non functional)