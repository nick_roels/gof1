package observer;

import java.util.Map;
import java.util.Observable;

public class MyObservable extends Observable {
    public void notifyObservers(String changed, Punt punt) {
        setChanged();
        notifyObservers( Map.entry(changed,punt));
    }


}

