package observer;

import java.util.*;

public class PuntObserver implements Observer{


  public void update(Observable observable, Object object) {
    Map.Entry entry = (Map.Entry ) object;
    System.out.println(entry.getKey() + " waarde gewijzigd, nieuwe waarden: " + entry.getValue());
  }
}