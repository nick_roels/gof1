package observer;

import java.util.Observable;
import java.util.Observer;

public class PuntObserver implements Observer {


    public void update(Observable observable, Object object) {
        System.out.println(object + " waarde gewijzigd, nieuwe waarden: " + observable);
    }
}