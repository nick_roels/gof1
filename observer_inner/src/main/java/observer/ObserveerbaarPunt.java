package observer;

import java.util.*;

public class ObserveerbaarPunt extends Punt {
    private MyObservable notifier = new MyObservable();

    class MyObservable extends Observable {
        public void notifyObservers(Object object) {
            super.setChanged();
            super.notifyObservers( object );
        }

        public Punt getPunt(){
            return ObserveerbaarPunt.this;
        }
    }

    public ObserveerbaarPunt(int x, int y) {
        super(x, y);
    }

    public void verdubbelX() {
        super.verdubbelX();
        notifier.notifyObservers("X");
    }

    public void verdubbelY() {
        super.verdubbelY();
        notifier.notifyObservers("Y");
    }

    public void addObserver(Observer observer) {
        notifier.addObserver(observer);
    }
}
