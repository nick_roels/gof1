package observer;

import java.util.*;

public class PuntObserver implements Observer {

	public void update(Observable observable, Object object) {
		ObserveerbaarPunt.MyObservable myObservable = (ObserveerbaarPunt.MyObservable) observable;
		System.out.println(object + " waarde gewijzigd, nieuwe waarden: " + myObservable.getPunt());
	}
}